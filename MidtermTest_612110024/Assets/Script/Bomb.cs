﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class Bomb : MonoBehaviourPun
{
    public float healingPoint = 0.3f;
    public float fillBars;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Test other : " + other.gameObject.name);
        if (other.gameObject.CompareTag("Player"))
        {
            PunHealth otherHeal = other.gameObject.GetComponent<PunHealth>();
            otherHeal.Healing(healingPoint);
            otherHeal.FillingBar(fillBars);
            
            photonView.RPC("PunRPCHealing",RpcTarget.MasterClient);
        }
    }

    [PunRPC]
    private void PunRPCHealing()
    {
        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        if(!photonView.IsMine)
            return;;
        PhotonNetwork.Destroy(this.gameObject);
    }
}
