﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(PhotonTransformView))]
public class PlayerControlScript : MonoBehaviourPunCallbacks
{
    public static GameObject LocalPlayerInstance;
    //public PhotonView photonView;
    public Rigidbody2D rb;
    public GameObject PlayerCamera;
    public SpriteRenderer sr;
    public Text PlayerNameText;

    public bool IsGrounded = false;
    public float MoveSpeed;
    public float JumpForce;
    public Transform feetPos;
    public float checkRadius;
    public LayerMask whatIsGround;

    public GameObject BulletObj;
    public Transform FirePos;

    public bool DisableInput = false;

    public int DeadCount = 2;
    
    private void Awake()
    {
        if (photonView.IsMine)
        {
            LocalPlayerInstance = gameObject;
            PlayerCamera.SetActive(true);
            PlayerNameText.text = PhotonNetwork.NickName;
        }
        else
        {
            PlayerNameText.text = photonView.Owner.NickName;
            PlayerNameText.color = Color.red;
        }
    }

    private void Update()
    {
        if (photonView.IsMine && !DisableInput)
        {
            CheckInput();
        }
    }

    private void CheckInput()
    {
        var move = new Vector3(Input.GetAxisRaw("Horizontal"), 0);
        transform.position += move * MoveSpeed * Time.deltaTime;
        IsGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, whatIsGround);

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Shoot();
        }
        
        if (Input.GetKeyDown(KeyCode.A))
        {
            photonView.RPC("FlipTrue",RpcTarget.All);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            photonView.RPC("FlipFalse", RpcTarget.All);
        }

        if (IsGrounded == true && Input.GetKeyDown(KeyCode.Space))
        {
            rb.velocity = Vector2.up * JumpForce;
        }
    }

    private void Shoot()
    {
        if (sr.flipX == false)
        {
            GameObject obj = PhotonNetwork.Instantiate(BulletObj.name, new Vector2(
                FirePos.transform.position.x
                , FirePos.transform.position.y), quaternion.identity, 0);
        }

        if (sr.flipX == true)
        {
            GameObject obj = PhotonNetwork.Instantiate(BulletObj.name, new Vector2(
                FirePos.transform.position.x
                , FirePos.transform.position.y), quaternion.identity, 0);
            obj.GetComponent<PhotonView>().RPC("ChangeDir_left", RpcTarget.All);
        }
    }
    
    [PunRPC]
    private void FlipTrue()
    {
        sr.flipX = true;
    }

    [PunRPC] 
    private void FlipFalse()
    {
        sr.flipX = false;
    }
}
