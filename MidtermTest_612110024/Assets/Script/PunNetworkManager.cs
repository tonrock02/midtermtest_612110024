﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class PunNetworkManager : MonoBehaviourPunCallbacks
{
    public static PunNetworkManager Instance;
    
    public GameObject PlayerPrefab;
    
    public GameObject GameCanvas;

    public GameObject SceneCamera;

    [HideInInspector] public GameObject LocalPlayer;
    public Text RespawnText;
    public GameObject RespawnMenu;
    private float TimeAmount = 3f;
    private bool RunTime = false;

    // public GameObject OverCon;
    public readonly byte CallEvent = 10;

    public GameObject CanvasGameover;
     // public override void OnEnable()
     // {
     //     base.OnEnable();
     //     PhotonNetwork.AddCallbackTarget(this);
     // }
     //
     // public override void OnDisable()
     // {
     //     base.OnDisable();
     //     PhotonNetwork.RemoveCallbackTarget(this);
     // }

    private void Awake()
    {
        Instance = this;
        GameCanvas.SetActive(true);
    }

    private void Update()
    {
        if (RunTime)
        {
            StartRespawn();
        }
        
    }

    public void EnabledRespawn()
    {
        TimeAmount = 3f;
        RunTime = true;
        RespawnMenu.SetActive(true);
    }
    
    
    private void StartRespawn()
    {
        TimeAmount -= Time.deltaTime;
        RespawnText.text = "Respawning count " + TimeAmount.ToString("F0");

        if (TimeAmount <= 0)
        {
            LocalPlayer.GetComponent<PhotonView>().RPC("Respawn",RpcTarget.All);
            LocalPlayer.GetComponent<PunHealth>().EnableInput();
            RespawnLocation();
            RespawnMenu.SetActive(false);
            RunTime = false;
        }
    }

    public void RespawnLocation()
    {
        float randomValue = Random.Range(-10, 10);
        LocalPlayer.transform.localPosition = new Vector2(randomValue, -0.5f);
    }
    
    public void SpawnPlayer()
    {
        //float randomValue = Random.Range(-1f, 1f);

        PhotonNetwork.Instantiate(PlayerPrefab.name, new Vector2(0f,1f), Quaternion.identity, 0);
        GameCanvas.SetActive(false);
        SceneCamera.SetActive(false);
    }

    public IEnumerator Gameover()
    {
        yield return new WaitForSeconds(0.1f);
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions {Receivers = ReceiverGroup.All};
        ExitGames.Client.Photon.SendOptions sendOptions=new ExitGames.Client.Photon.SendOptions{Reliability = true};
        PhotonNetwork.RaiseEvent(CallEvent, null, raiseEventOptions, sendOptions);
    }

    public void IsgameOver()
    {
        CanvasGameover.SetActive(true);
    }
    // private void CallRaiseEvent()
    // {
    //     object[] content = new object[] {PunHealth.OverCondi=false};
    //     RaiseEventOptions raiseEventOptions = new RaiseEventOptions {Receivers = ReceiverGroup.All};
    //     SendOptions sendOptions = new SendOptions {Reliability = true, Encrypt = true};
    //     PhotonNetwork.RaiseEvent()
    //
    // }
}
