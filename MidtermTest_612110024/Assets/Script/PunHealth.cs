﻿using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

public class PunHealth : MonoBehaviourPunCallbacks, IPunObservable, IOnEventCallback
{
    public Image HealthBar;
    public float HealthAmount;

    public Rigidbody2D rb;
    public BoxCollider2D bc;
    public SpriteRenderer sr;

    public PlayerControlScript plc;
    public GameObject PlayerCanvas;


    private void Awake()
    {
        if (photonView.IsMine)
        {
            PunNetworkManager.Instance.LocalPlayer = this.gameObject;
        }
    }

    [PunRPC]
    public void DecreaseHealth(float amount)
    {
        ModifyHealth(amount);
    }

    private void CheckHealth()
    {
        HealthBar.fillAmount = HealthAmount / 100f;
        if (photonView.IsMine && HealthAmount <= 0 && plc.DeadCount > 0)
        {
            PunNetworkManager.Instance.EnabledRespawn();
            plc.DisableInput = true;
            this.GetComponent<PhotonView>().RPC("Dead",RpcTarget.All);
            //Debug.Log("Killed by :" + photonView.ViewID);
        }
        if (photonView.IsMine && plc.DeadCount == 0)
        {
            plc.DisableInput = true;
            if (HealthAmount <= 0)
            {
                StartCoroutine(PunNetworkManager.Instance.Gameover());
            }
            Debug.Log("Dead");
        }

        // if (photonView.IsMine && plc.DeadCount < -1 && HealthAmount <= 0)
        // {
        //     StartCoroutine(PunNetworkManager.Instance.Gameover());
        // }
    }

    public void EnableInput()
    {
        plc.DisableInput = false;
    }

    [PunRPC]
    private void Dead()
    {
        rb.gravityScale = 0;
        bc.enabled = false;
        sr.enabled = false;
        PlayerCanvas.SetActive(false);
        plc.DeadCount -= 1;

    }
    
    [PunRPC]
    private void Respawn()
    {
        rb.gravityScale = 2;
        bc.enabled = true;
        sr.enabled = true;
        PlayerCanvas.SetActive(true);
        HealthBar.fillAmount = 1f;
        HealthAmount = 100f;
    }
    
    private void ModifyHealth(float amount)
    {
        if (photonView.IsMine)
        {
            HealthAmount -= amount;
            HealthBar.fillAmount -= amount;
        }
        else
        {
            HealthAmount -= amount;
            HealthBar.fillAmount -= amount;
        }
        
        CheckHealth();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(HealthAmount);
            //stream.SendNext(HealthBar);
        }
        else
        {
            HealthAmount = (float) stream.ReceiveNext();
            //HealthBar.fillAmount = (float) stream.ReceiveNext();
        }
    }

    public void Healing(float amout)
    {
        HealthAmount += amout;//
    }
    
    public void FillingBar(float fill)
    {
        HealthBar.fillAmount += fill;
    }

    public void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == PunNetworkManager.Instance.CallEvent)
        {
            PunNetworkManager.Instance.IsgameOver();
        }
    }

    
}
